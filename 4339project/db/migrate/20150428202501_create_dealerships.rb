class CreateDealerships < ActiveRecord::Migration
  def change
    create_table :dealerships do |t|
      t.integer :salestaxtotals
      t.integer :netprofit
      t.integer :salestaxprofit

      t.timestamps null: false
    end
  end
end
