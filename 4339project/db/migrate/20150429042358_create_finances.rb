class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.integer :price
      t.integer :interest
      t.integer :year_plan

      t.timestamps null: false
    end
  end
end
