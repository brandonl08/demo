class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.string :firstname
      t.string :lastname
      t.integer :number
      t.date :date
      t.integer :price
      t.string :vin
      t.string :status

      t.timestamps null: false
    end
  end
end
