class Quote < ActiveRecord::Base
  belongs_to :car
  has_many :sale
  has_many :dealerships
  has_many :car
  has_many :finance
  has_one :staff
end
