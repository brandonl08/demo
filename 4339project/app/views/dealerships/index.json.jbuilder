json.array!(@dealerships) do |dealership|
  json.extract! dealership, :id, :salestaxtotals, :netprofit, :salestaxprofit
  json.url dealership_url(dealership, format: :json)
end
