json.array!(@cars) do |car|
  json.extract! car, :id, :model, :color, :vin, :status
  json.url car_url(car, format: :json)
end
