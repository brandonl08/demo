json.array!(@finances) do |finance|
  json.extract! finance, :id, :price, :interest, :year_plan
  json.url finance_url(finance, format: :json)
end
