# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updateNotes = ->
  selection_id = $('#quote_price').val()
  $.getJSON '/quote/' + selection_id + '/notes', {},(json, response) ->
    $('#notes').text json['notes']

validatePrice = ->
  if (parseFloat($('#quote_price').val()) <= 0.0) or ($('#quote_price').val() == "")
    alert "You must enter a price that is greater greater than 0."
    return false

$ ->

if $('#quote_price').length > 0
  updateNotes()
$(document).on 'submit',validatePrice()
$(document).on 'keyup', '#quote_price',->
  if $(this).val() < 1
    $('#price_warning').text("Enter greater than zero!")
  else
    $('#price_warning').text("")